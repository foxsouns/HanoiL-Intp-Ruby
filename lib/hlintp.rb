#!/usr/bin/env ruby
# Hopefully an interpreter for Hanoi Love.
# Usage: hlintp.rb file.hl
# This is a work in progress. Be prepared. I'm not.

Encoding::default_internal = Encoding::default_external = "ASCII-8BIT"

module HanoiLove
  # ResultStruct = Struct.new :stdout, :stack, :exitcode
  def self.run code, stdout = StringIO.new, stdin = STDIN

    # # Record when this starts, for specs after run
    # msec = Time.now.to_f * 1000;

    cp = -1 # code pointer

    code.delete "^.',;`:!\""  # Hanoi Love commands are as follows: . ' , ; ` "  : !

    # Make cl exist now
    cl = code.length # cleaned code length

    # HL specific stuff
    rg = 0 # value in register (0-255)
    sa, sb, sc, sd = [], [], [], [] 
    ss = [sa, sb, sc, sd] # stack list (for A-D)
    st = 0 # stack number for scack changing

    iomode = false # i need to look back at this l8r

    # Interpreting! I work on this through repl.it, and it fucks up quotes when using ' if" ', thus the comments and newline for the quote characters, to keep "proper formatting" when viewed on replit.
    until cl == cp += 1
      case code[cp]
        when ?. # next stack: selects next stack in order (A,B,C,D). loops back to A from D.
          then st = (st + 1) % 4
        when ?' # TODO: if stack D then push location pointer -1 to stack D; 
           then iomode ? stdout.print(rg.chr) : ss[st].push(rg)                  
        when ?, # TODO: if stack D then pop above pushed location from D
          then rg = (iomode ? stdin.getbyte : ss[st].pop || (st == 0 ? 1 : 0)) 
        when ?; 
          then 
          #[psuedocode] if not stack d, if not iomode, pop current stack and add to register, loop to zero if above 255: pop from iomode add to register: pop stack d
        when ?` # TODO: pop from stack sub from register: unless stack D, then pop location and noop
          then 
          #[psuedocode] if not stack d, if not iomode, pop current stack and sub from register, loop to 255 if below 0: pop from iomode sub from register: pop stack d
        when ?" 
          then iomode ^= true
        when ?: 
          then 
          # skip if zero: if the register is zero, skip instructions until the "!" instruction is reached.
        when ?! 
          then 
          # end skip/halt: if matching a ":", end skip. if not matching a ":", halt.
      end
    end

    stdout

  end
end
